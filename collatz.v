Require Import ZArith List.

Open Scope Z_scope.

Definition Collatz_step (x : Z) :=
  if (x mod 2) =? 0 then
    x / 2
  else
    3 * x + 1.

Definition Collatz (x : Z) :=
  if x =? 1 then x else Collatz_step x.

Compute Z.iter 1000 Collatz 43.

Definition Zseq x y := List.map Z.of_nat (seq x y).

Compute Zseq 2 99.

Compute find (fun x => negb (Z.iter 100 Collatz x =? 1)) (Zseq 2 99).

Compute find (fun x => negb (Z.iter 1000 Collatz x =? 1)) (Zseq 2 10000).

Compute Z.iter 1000 (fun '(x, l) => 
                      if x =? 1 then (x, l) else
                        (Collatz_step x, x :: l)) (27, nil).

Compute length (snd (Z.iter 1000 (fun '(x, l) => 
                      if x =? 1 then (x, l) else
                        (Collatz_step x, x :: l)) (27, nil))).

