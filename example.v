(* Preparatory work, not for the eyes of students. *)

Require Import Reals Coquelicot.Coquelicot Interval.Tactic Lra.

Require Import Classical.

Open Scope R_scope.

Ltac Fix name := 
  match goal with
   | |- _ -> _ => fail "statement should be a universal quantification"
   | |- forall _, _ => intros name
   end.

Ltac assume f :=
  match goal with |- _ -> ?c =>
  let x := fresh "assumed_fact" in
  assert (x : f -> c);[intros x | exact x]
  end.

Ltac check :=
  solve[auto | tauto | field; lra | lra | interval | nra] ||
        fail "I can't manage to check this".

Ltac now_prove f :=
  let x := fresh "now_prove_step" in enough (x : f) by exact x.

Definition derivative : (R -> R) * R * R -> Prop :=
  fun (tuple : (R -> R) * R * R) =>
  is_derive (fst (fst tuple)) (snd (fst tuple)) (snd tuple).


Ltac derivative_prover := 
  unfold derivative; simpl; auto_derive; check.

Lemma Mean_Value_Theorem :
   forall (f f' : R -> R) (a b : R),
   a < b /\ (forall c, a <= c <= b -> derivative(f, c, f' c)) ->
   exists c, f(b) - f(a) = f'(c) * (b - a) /\ a < c < b.
Proof.
intros f f' a b [altb ff']; apply MVT_cor2; auto.
unfold derivative in ff'; simpl in ff'.
now intros c cint; rewrite <- is_derive_Reals; apply ff'.
Qed.

Lemma cos_decreasing x y :
  0 <= x <= PI /\ 0 <= y <= PI /\ x < y -> cos y < cos x.
Proof.
intros; apply cos_decreasing_1; tauto.
Qed.

Lemma cos_decreasing' x y :
 0 <= x <= PI /\ 0 <= y <= PI -> x < y <-> cos y < cos x.
Proof.
intros; split.
  apply cos_decreasing_1; tauto.
apply cos_decreasing_0; tauto.
Qed.

(* End of preparatory work. *)

Lemma pi_short_approx : 3.14 < PI < 3.15.
Proof.
interval_intro PI.
lra.
(* just calling interval would have done
  the trick. *)
Qed.

Lemma strange_number : tan (9 ^ 9) < 0.
Proof.
Fail interval_intro (tan (9 ^ 9)).
Compute (9 ^ (9 ^ 9))%Z.
assert (int: 9 ^ 9 = IZR (387420489)).
lra.
rewrite int.
interval_intro (tan 387420489).
lra.
Qed.

Lemma sin_gt_x x : 0 < x -> sin x < x.
Proof.
intros xgt0.
assert (0 < x <= 1 -> sin x < x).
  intros xint.
  Search sin 0.
  assert (sin 0 = 0) by apply sin_0.
  enough (0 < (x - sin x) - (0 - sin 0)) by lra.
  Check Mean_Value_Theorem.
  assert (exists c, (x - sin x) - (0 - sin 0) =
                      (1 - cos c) * (x - 0) /\
             (0 < c < x)) as [c [diff cint]].
    apply (Mean_Value_Theorem (fun y => y - sin y)
               (fun z => 1 - cos z)).
    split.
      lra.
    intros c cint; derivative_prover.
  rewrite diff.
  Search (0 < _ * _).
  apply Rmult_lt_0_compat.
    Search cos 0.
    rewrite <- cos_0.
    Search cos (_ < _).
    enough (cos c < cos 0) by lra.
    interval_intro PI.
  apply cos_decreasing_1; lra.
  lra.
assert (1 < x -> sin x < x).
  enough (sin x <= 1) by lra.
  Search (sin _ <= 1).
  assert (tmp := SIN_bound x).
  lra.
lra.
Qed.
 
Lemma sin_gt_x2 x : 0 < x -> sin x < x.
Proof.
Fail check.
assert (right_part : 1 < x -> sin x < x).
  assert (sin x <= 1)              by check.
  check.
(* The next command shows what would happen if a student
  wanted to progress in a way that is not complete. *)
Fail enough (left_part : 0 < x < 1 -> sin x < x) by check.
enough (left_part : 0 < x <= 1 -> sin x < x)   by check.
assume (0 < x <= 1).
now_prove (sin x < x).
(* Starting preparatory work to use the mean value theorem *)
pose (f := fun y => y - sin y).
assert (f(0) = 0).
    unfold f.
    assert (sin 0 = 0) by apply sin_0.
    check.
enough (0 < f(x))
    by (unfold f in *; check).
enough (0 < f(x) - f(0))
    by check.
assert (exists c, (f(x) - f(0) = (1 - cos c) * (x - 0)) /\
          0 < c < x) as [c derivative_in_c_and_c_between_a_x].
    apply Mean_Value_Theorem.
    now_prove (0 < x /\
               forall c, 0 <= c <= x -> derivative (f, c, 1 - cos c)).
    split.
        check.
    now_prove (forall c, 0 <= c <= x -> derivative (f, c, 1 - cos c)).
    Fix c.
    assume (0 <= c <= x).
    now_prove (derivative(f, c, 1 - cos c)).
    unfold f.
    derivative_prover.
now_prove (0 < f(x) - f(0)).
enough (0 < (1 - cos c) * (x - 0)) by check.
enough (cos c < 1) by check.
assert (cos 0 = 1) by apply cos_0.
enough (cos c < cos 0) by check.
apply cos_decreasing.
now_prove (0 <= 0 <= PI /\ 0 <= c <= PI /\ 0 < c).
Fail check.
repeat split; try check.
now_prove (c <= PI).
assert (3.14 <= PI <= 3.15) by check.
check.
Qed.

Section sum_n_first_natural_numbers.

Variable isnat : R -> Prop.

Hypothesis isnat0 : isnat 0.

Hypothesis isnat1 : isnat 1.

Hypothesis isnat_add : forall x y, isnat x /\ isnat y -> isnat (x + y).

Hypothesis isnat_ind : 
  forall P : R -> Prop,
    (P 0 /\
     (forall x, isnat x /\ P x -> P (x + 1))) ->
  forall x, isnat x -> P x.

Ltac prove_by_nat_induction := apply isnat_ind.

Variable sumn : R -> R.

Hypothesis sumn0 : sumn 0 = 0.

Hypothesis sumn_1 : forall n, sumn (n + 1) = sumn n + n.

Lemma sumn_eq : forall n, isnat n -> sumn n = n * (n - 1) / 2.
Proof.
prove_by_nat_induction.
assert (base_case : sumn 0 = 0 * (0 - 1) / 2).
    check.
assert (step_case : forall n, (isnat n /\ sumn n = n * (n - 1) / 2) ->
                    sumn (n + 1) = (n + 1) * ((n + 1) - 1) / 2).
    Fix n.
    assume (isnat n /\ sumn n = n * (n - 1) / 2).
    now_prove (sumn (n + 1) = (n + 1) * ((n + 1) - 1) / 2).
    assert (eq_for_n : sumn n = n * (n - 1) / 2) by check.
    remember (sumn (n + 1)) as computation eqn: cval.
    assert (cval1 : computation = sumn n + n).
        rewrite cval, sumn_1; check.
    assert (cval2 : computation = (n * (n - 1) / 2 + n)).
        rewrite cval1, eq_for_n; check.
    check.
check.
Qed.

Section bigop.

Variable bigop : forall [T T' : Type] (id0 : T')
  (op : T' -> T' -> T') (s : T -> Prop) (F : T -> T'), T'.

Variable nat_segment : R -> (R -> Prop).

Hypothesis nat_segment_def :
  forall n, 
  isnat n -> forall m, (nat_segment n m) <-> (m < n /\ isnat m).

Lemma isnat_ge0 x : isnat x -> 0 <= x.
Proof.
revert x; apply isnat_ind; split.
  check.
intros x it; check.
Qed.

Variable finite : forall [T : Type], (T -> Prop) -> Prop.

Hypothesis finite_0 : forall [T : Type](s : T -> Prop),
  (forall x, ~ s x) <-> finite s.

Hypothesis finite_1 : forall [T : Type](x : T),
  finite (fun y => x = y).

Hypothesis finite_U : forall [T : Type](s1 s2 : T -> Prop),
  finite s1 /\ finite s2 -> finite (fun x => s1 x \/ s2 x).

Hypothesis eq_finite : forall [T : Type] (s1 s2 : T -> Prop),
  (forall x, s1 x <-> s2 x) -> (finite s1 <-> finite s2).

Variable card : forall [T : Type], (T -> Prop) -> R.

Lemma nat_segment_0_empty : forall x, ~ nat_segment 0 x.
Proof.
intros x; rewrite nat_segment_def; auto.
intros [xlt0 natx]; assert (tmp := isnat_ge0 _ natx); lra.
Qed.

Lemma isnat_discrete x : isnat x ->
   forall y, isnat y -> x <= y < x + 1 -> x = y.
Proof.
revert x.
apply (isnat_ind (fun u => forall y, isnat y -> u <= y < u + 1 -> u = y)).
split.
apply (isnat_ind (fun u => 0 <= u < 0 + 1 -> 0 = u)).
  split;[auto | ].
  intros y [naty _] abs; assert (tmp := isnat_ge0 y naty); lra.
intros x [natx Ih].
apply (isnat_ind (fun y => x + 1 <= y < x + 1 + 1 -> x + 1 = y)).
split.
  assert (tmp := isnat_ge0 _ natx); lra.
intros y [naty _] cond.
enough (x = y) by lra.
apply Ih; auto; lra.
Qed.

Lemma nat_segment_step n : isnat n -> forall y,
  nat_segment (n + 1) y <-> nat_segment n y \/ y = n.
Proof.
intros natn y.
rewrite nat_segment_def; auto.
rewrite nat_segment_def; auto.
split.
  intros [yltxp1 naty].
  case (Rlt_dec y n) as [yltx | ygex].
    now left; split.
    right.
  now apply eq_sym; apply isnat_discrete; auto; lra.
intros [[yltx naty] | yisx].
  now split;[lra | auto].
now split;[lra | rewrite yisx; auto].
Qed.

Lemma finite_nat_segment n : isnat n -> finite (nat_segment n).
Proof.
revert n.
apply isnat_ind.
assert (finite (nat_segment 0)).
  now apply finite_0; apply nat_segment_0_empty.
assert (forall x, isnat x /\
           finite (nat_segment x) -> finite (nat_segment (x + 1))).
  intros x [natx Ih].
  apply (eq_finite _ _ (nat_segment_step x natx)); apply finite_U; split; auto.
  apply (eq_finite (fun y => x = y)).
    now intros y; split; intros one_eq; rewrite one_eq.
  now apply finite_1.
now split; auto.
Qed.

Class monoid (T : Type) (zero : T) (op : T -> T -> T) := {
  zero_neutral_l : forall x, op zero x = x;
  sero_neutral_r : forall x, op x zero = x;
  op_associative : forall x y z, op x (op y z) = op (op x y) z;
  op_commutative : forall x y, op x y = op y x
}.

Variable bigop_0 : forall [T T' : Type](zero : T') (op : T' -> T' -> T')
  (s : T -> Prop) (F : T -> T'),
  (forall x, ~ s x) -> bigop zero op s F = zero.

Variable bigop_rec: forall [T T' : Type](zero : T') (op : T' -> T' -> T')
   `{monoid T' zero op}
   (a : T) (s : T -> Prop) (F : T -> T'),
   (~ s a) ->
   finite s ->
   bigop zero op (fun y => s y \/ y = a) F =
   op (bigop zero op s F) (F a).

Variable eq_set_bigop :
forall [T T' : Type](zero : T') (op : T' -> T' -> T')
   `{monoid T' zero op}
   (s1 s2 : T -> Prop) (F : T -> T'),
   (forall y, s1 y <-> s2 y) ->
   bigop zero op s1 F = bigop zero op s2 F.

Instance monoid_plus : monoid R 0 Rplus.
Proof.
constructor.
  exact Rplus_0_l.
  exact Rplus_0_r.
  exact (fun x y z => eq_sym (Rplus_assoc x y z)).
  exact Rplus_comm.
Qed.

Lemma sumn_bigop n :  isnat n ->
  sumn n = bigop 0 Rplus (nat_segment n) (fun i => i).
Proof.
revert n; apply isnat_ind.
assert (sumn 0 = bigop 0 Rplus (nat_segment 0) (fun i => i)).
  rewrite sumn0.
  now rewrite bigop_0; auto; apply nat_segment_0_empty.
assert(forall x,
   isnat x /\ sumn x = bigop 0 Rplus (nat_segment x) (fun i : R => i) ->
   sumn (x + 1) = bigop 0 Rplus (nat_segment (x + 1)) (fun i : R => i)).
intros x [natx Ih].
  rewrite (eq_set_bigop 0 Rplus (nat_segment (x + 1))
             (fun y => nat_segment x y \/ y = x)
             (fun i => i) (nat_segment_step x natx)).
  rewrite bigop_rec.
        now rewrite sumn_1, Ih.
      now typeclasses eauto.
    rewrite nat_segment_def; auto; intros [xltx _]; lra.
  now apply finite_nat_segment.
tauto.
Qed.

Hypothesis bigop_ext : forall (f g : R -> R) op z (s : R -> Prop),
  (forall x, s x -> f x = g x) ->
  bigop z op s f = bigop z op s g.

Example big3 f (s : R -> Prop) :
  bigop 0 Rplus (nat_segment 3) f = f 0 + f 1 + f 2.
Proof.
assert (n3 : isnat 3).
  replace 3 with (1 + 1 + 1) by  ring.
  check.
assert (n2 : isnat 2).
  replace 2 with (1 + 1) by ring.
  check.
rewrite (eq_set_bigop 0 Rplus _ (fun x => nat_segment 2 x \/ x = 2)).
rewrite bigop_rec; try typeclasses eauto.
rewrite (eq_set_bigop 0 Rplus _ (fun x => nat_segment 1 x \/ x = 1)).
rewrite bigop_rec; try typeclasses eauto.
rewrite (eq_set_bigop 0 Rplus _ (fun x => nat_segment 0 x \/ x = 0)).
rewrite bigop_rec; try typeclasses eauto.
rewrite bigop_0.
ring.
apply nat_segment_0_empty.
rewrite nat_segment_def.
check.
check.
intros y; replace 1 with (0 + 1) by ring; apply nat_segment_step.
check.
rewrite nat_segment_def; check.
intros y; replace 2 with (1 + 1) by ring; apply nat_segment_step.
check.
rewrite nat_segment_def; check.
intros y; replace 3 with (2 + 1) by ring; apply nat_segment_step.
check.
Qed.

Notation "\sum_( x 'in s ) [ F ]" :=
    (bigop 0 Rplus s (fun x => F)).


(* Notation "\sum_( 0 <= x < n ) [ F ]" :=
  (bigop (nat_segment n) (fun x => F)).
*)

Lemma sum_geom n : 
  \sum_(x 'in nat_segment n) [x] = n * (n + 1) / 2.
Proof.

