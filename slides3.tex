\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\definecolor{myblue}{rgb}{0.1,0.1,0.6}
\definecolor{myblue2}{rgb}{0.3,0.3,0.8}
\definecolor{darkgreen}{rgb}{0.01,0.3,0.0}
\setbeamercolor{coloredboxstuff}{fg=white,bg=myblue2}
\newcommand{\coqand}{\texttt{/\char'134}}
\newcommand{\coqor}{\texttt{\char'134/}}
\setbeamertemplate{footline}[frame number]
\title{Numbers in Coq}
\author{Yves Bertot}
\date{June 2023}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{The situation of numbers in the Coq system}
Computation is a strong design attractor for Coq
\begin{itemize}
\item Incentive to reason about algorithms
\item Being able to run these algorithms is nice
\item Even more so with reflective tactics
\begin{itemize}
\item Tacticts that rely on internal computation
\end{itemize}
\end{itemize}
Many choices of data structures for representing integers and natural numbers
\begin{itemize}
\item Peano approach: 0 and successors
\item base and position representations
\begin{itemize}
\item Sequences of digits or bits
\item Preference for binary
\item Also possible to use binary tree representations
\end{itemize}
\end{itemize}
Ease of programming dependent on the choice of data structure
\begin{itemize}
\item Bare metal inductive type theory
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Most advanced: computation with real numbers}
WARNING: not available in JsCoq

\begin{itemize}
\item Challenge: a pocket calculator says that \(e = 2.71828182\ldots\) and \(\pi = 3.14159265\ldots\)

\item What is the sign of \(3.14159265 e -  2.71828182 \pi\) ?
\end{itemize}

\begin{verbatim}
Require Import Reals Interval.Tactic Lra.

Open Scope R_scope.

Lemma real_exercise :
   0 < 3.14159265 * exp 1 -  2.71828182 * PI.
Proof.  interval. Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{The case of real numbers}
Real numbers outside of the Coq-computable world
\begin{itemize}
\item Proof-based computation is still available
\item Computing approximations
\item {\em Fallible} computations
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{How does it work?}
\begin{itemize}
\item Real numbers are in a type ``assumed to exist'', with 0, 1, +, \dots, and
complete archimedian field properties,
\item \(e^x\) is defined as \(\sum_{i = 0}^{+\infty} \frac{x^i}{i!}\)
\item \(\cos x\) is defined as \(\sum_{i = 0}^{+\infty} \frac{(-1)^ix ^ {2 i}}{(2 i)!}\)
\item \(PI\) is defined as twice the first positive root of \(\cos\).
\item An extra theorem shows \(PI = 4 ( 4 atan \frac{1}{5} - atan\frac{1}{239})\)
\item If the input is rational, each power series computation only uses
 rational numbers
\item Intervals are computed at each step, and then combined
\item The method has weaknesses, but works well in many cases
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Weakness of the interval approach}
\begin{verbatim}
Require Import Reals Interval.Tactic Lra.

Open Scope R_scope.

Lemma real_exercise2 x :
   1 / 2 ^ 10 <= x <= 1 -> 0 <= x - sin x.
Proof.
intros xint.
interval_intro (x - sin x) with
   (i_decimal, i_prec 120, i_bisect x, i_depth 8).
(* Long time of computation, result lower bound below 0 *)
interval_intro (x - sin x) with
   (i_decimal, i_taylor x, i_prec 120, i_bisect x, i_depth 10).
lra.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Didactic issues with {\tt interval}}
\begin{itemize}
\item Does not let the student practice skills
\item Domain of practical application difficult to understand
\begin{itemize}
\item This requires acquiring some skills, non-mathematical
\end{itemize}
\item Useful to have for menial goals, but it feels too powerful
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Rational numbers}
\begin{itemize}
\item Use of the {\tt Compute} command.

\item Rational numbers are encoded as pairs of a signed integer and
 a positive number

\item Exact operations are provided (no square root)

\item Results are not normalized (for efficency reason)

\item The normalization function must be called explicitely

\item There is a specific equality for rational numbers, noted {\tt ==}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Rational numbers are under-appreciated}
\begin{itemize}
\item There are comparatively less theorems than for real numbers or integers
\item Equality between rational numbers is only treated as an equivalence
relation
\item Naked eye comparison is uncomfortable
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Binary integers}
\begin{itemize}
\item A specific datatype to represent positive integers (no size limit)
\item A wrapper to add signs: two types {\tt positive} and {\tt Z}
\item Clumsy recursion: recursive calls are only available for half numbers
\begin{itemize}
\item Good enough for usual operations: +, *, /, square root, 
\item Clumsy for gcd, factorial
\end{itemize}
\item Proof by induction requires more skill than for natural numbers
\item The workhorse for many computation tools in Coq, including numeral
notations in real numbers
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Examples with integers}
\begin{verbatim}
Require Import ZArith.
Open Scope Z_scope.

Check xO (xI xH).
  (*representation for 6, as positive number*)

Check Zpos (xO (xI xH)). 
  (*representation for 6, as a signed integer *)

Definition Zfactorial (x : Z) :=
  snd (Z.iter x (fun '(x, f) => (x + 1, f * x)) (1, 1)).

Compute Zfactorial 6. (* returns 720 as expected *)

Compute Zfactorial 100.
  (* returns a huge number, no notable delay *)
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Example proof with integers}
\begin{itemize}
\item \(\sqrt{2}\) is not rational
\item Rephrased with integers : \(\forall p q, 0 < q < p \rightarrow 2 * q ^ 2 = p ^ 2 \rightarrow {\tt False} \)
\item {\bf sketch of the proof}, if \(p\) is a minimal integer such that the equality
holds, then \(p\) is even, and then the equality
holds for \(q\) and \(p/2\)
\item The key step is that if the square of \(p\) is even, then \(p\) is even,
this take some time to express with existing theorems.
\item Untold assumptions in this sketch are that p and q are positive (in particular, non-zero)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Demo time}
\begin{center}
\LARGE DEMO
\end{center}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
