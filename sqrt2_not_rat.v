Require Import ZArith Zwf Lia.
Open Scope Z_scope.

Lemma sqrt2_not_rat_pos : forall p q, 0 < q < p -> 2 * q ^ 2 = p ^ 2 -> False.
Proof.
induction p as [p Ih] using (well_founded_ind (Zwf_well_founded 0)).
intros q qgt0 eqsqrt.
assert (pdiv2 : p = 2 * (p / 2)).
  assert (peven : Z.even p = true).
    assert (tmp : Z.even (p ^ 2) = true).
      rewrite <- eqsqrt, Zeven_mod, Z.mul_mod, Z_mod_same_full; easy.
    rewrite Z.even_pow in tmp; easy.
  assert (tmp := Z_div_mod_eq_full p 2).
  rewrite Zeven_mod, <- Zeq_is_eq_bool in peven; lia.
replace (p ^ 2) with (2 * (2 * (p / 2) ^ 2)) in eqsqrt by
 (rewrite pdiv2 at 2; ring).
assert (eqsqrt' : 2 * (p / 2) ^ 2 = q ^ 2) by lia.
assert (qltp : Zwf 0 q p) by (unfold Zwf; lia).
apply (Ih q qltp (p / 2)); auto.
assert (hpgt0 : 0 < p / 2) by lia.
split; [lia | ].
assert (sqr_cmp : (p / 2) ^ 2 < q ^ 2).
  enough (0 < (p / 2) ^ 2) by lia.
  replace 0 with (0 ^ 2) by ring.
  rewrite <- Z.pow_lt_mono_l_iff; lia.
rewrite <- Z.pow_lt_mono_l_iff in sqr_cmp; lia.
Qed.

(* For a purist, the previous statement is not complete, since it
 assumes p and q to be positive.  Making the statement complete makes
 it tedious to complete. *)
Lemma sqrt2_not_rat : forall p q, 2 * q ^ 2 = p ^ 2 -> p = 0.
Proof.
assert (Z.Even 2) by now rewrite <- Z.even_spec.
intros p q eqsqr.
assert (q0top : q = 0 -> p = 0) by nia.
case (Z_lt_le_dec p 0). 
  case (Z_lt_le_dec q 0).
    intros qlt0 plt0; case (sqrt2_not_rat_pos (-p) (-q)).
      split;[lia | ].
      assert (q2ltp2 : q ^ 2 < p ^ 2).
        enough (q2gt0 : 0 < (- q) ^ 2).
          rewrite Z.pow_opp_even in q2gt0; auto.
          lia.
        apply Z.pow_pos_nonneg; lia.
      rewrite <- (Z.pow_opp_even q), <- (Z.pow_opp_even p) in q2ltp2; auto.
      rewrite <- Z.pow_lt_mono_l_iff in q2ltp2; lia.
    now rewrite !Z.pow_opp_even; auto.
  intros qge0 plt0.
  assert (q2ltp2 : q ^ 2 < p ^ 2).
    enough (q2gt0 : 0 < q ^ 2) by lia.
    apply Z.pow_pos_nonneg; lia.
  rewrite <- (Z.pow_opp_even p) in q2ltp2; auto.
  rewrite <- Z.pow_lt_mono_l_iff in q2ltp2; try lia.
  destruct (sqrt2_not_rat_pos (- p) q); nia.
enough (0 < p -> p = 0) by lia.
intros pgt0.
destruct (Z_lt_le_dec q 0) as [qlt0 | qge0].
  destruct (sqrt2_not_rat_pos p (-q)); nia.
enough (0 < q -> p = 0) by lia.
intros qgt0.
assert (q2ltp2 : q ^ 2 < p ^ 2) by lia.
rewrite <- Z.pow_lt_mono_l_iff in q2ltp2; try lia.
destruct (sqrt2_not_rat_pos p q); lia.
Qed.
