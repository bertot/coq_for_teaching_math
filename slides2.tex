\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\definecolor{myblue}{rgb}{0.1,0.1,0.6}
\definecolor{myblue2}{rgb}{0.3,0.3,0.8}
\definecolor{darkgreen}{rgb}{0.01,0.3,0.0}
\setbeamercolor{coloredboxstuff}{fg=white,bg=myblue2}
\newcommand{\coqand}{\texttt{/\char'134}}
\newcommand{\coqor}{\texttt{\char'134/}}
\setbeamertemplate{footline}[frame number]
\title{The challenges of using Type Theory to teach Mathematics}
\author{Yves Bertot}
\date{June 2023}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Successes of Proof Assitants towards mathematics}
\begin{itemize}
\item Proof assistants: Coq, Isabelle/HOL, HOL, Hol-Light, Lean
\item Mathematics: number theory, algebra, Mathematical Analysis, 
\begin{itemize}
\item With applications in mind (cryptography, computer arithmetics)
\item Towards Computer-aided Proof (4 color, Kepler Conjecture, Ternary Goldbach conjecture)
\item Towards clarifications of mathematics (Perfectoid, Liquid Tensor Experiment)
\end{itemize}
\item Comprehensive libraries : mathlib, mathematical components
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Can these tools help students of mathematics}
\begin{itemize}
\item Students need to learn the language of mathematics
\begin{itemize}
\item As spoken by mathematicians
\item The language does not contain only grammar and spelling rules
\end{itemize}
\item Learning a computer-based tool adds to the load
\item This talk will focus on Type-theory modeling of the language
\begin{itemize}
\item Finding information
\item Handling variable binding
\item Impact of Curry-Howard
\item The distance from Set Theory
\item The manipulation of numbers 
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The question of libraries: a variety of approaches}
A personal perception: advertised advantages, hidden drawbacks
\begin{itemize}
\item A question independent from the prover's foundation
\item Mizar: a single mathematical library
\begin{itemize}
\item viewed as a publication process in a journal
\item Revision is possible (improvements of coding style, generalization of theorems, rare reorganizations)
\item stable software
\end{itemize}
\item Isabelle Archive of formal proof
\begin{itemize}
\item Maintainers of the library ensure continued existence through revisions
the proof assistant
\item A single web front gives access to all roots of developments, but
there maybe duplications of theorems between several developments
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The question of libraries (2)}
A more critical point of view
\begin{itemize}
\item Coq's model of libraries changed over time
\begin{itemize}
\item Donated contributions used to be maintained by Coq developers
\item No selection process apart from completeness
\item Libraries used to serve as context for salient theorems
\end{itemize}
\item New model relies on community effort
\begin{itemize}
\item Not all libraries are equal
\item Different libraries may have ovelapping purposes
\end{itemize}
\item Organizing the content is the subject of current reflections
\item Search is difficult
\begin{itemize}
\item Search by pattern through definitions is difficult
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Searching}
\begin{itemize}
\item Upon request by the user, provide collections of theorems that
are relevant to the problem
\item Too much information is counterproductive
\item Current choice : take incomplete terms from the user, substrings of the
name
\begin{itemize}
\item A skill to acquire
\item Dependent on naming conventions
\item Dependent on Mathematical culture : {\em Mean Value Theorem}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Illustration: search difficulty}
Example in the context of the mathematical components library.

The users see:
\begin{verbatim}
  x * (2 * y + 3 * z)
\end{verbatim}
They would like to transform this into:
\begin{verbatim}
x * (2 * y) + x * (3 * z)
\end{verbatim}
The theorem may be stored in memory as :
\begin{verbatim}
mulrDl : right_distributive mul add
\end{verbatim}
In this case, just expanding the definition of {\tt right\_distributive}
should help
\end{frame}
\begin{frame}
\frametitle{Next generation search}
\begin{itemize}
\item In the Isabelle/HOL world, {\tt sledgehammer} reduces the need for
search
\begin{itemize}
\item Basically dictates solutions to the user
\end{itemize}
\item Much excitement for Large Language Models
\item Tension between practicality and long term retention
\end{itemize}
\end{frame}
\begin{frame}
\begin{center}
\usebeamercolor[fg]{frametitle}{\Large Logical connectives and first-order proof}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Learning about logical connectives and quantifiers}
\begin{itemize}
\item One of the obvious places where proof assistants could help
\item Two activities could be performed using proof assistants:
\begin{enumerate}
\item Learning to write formulas that mirror the meaning of natural language
sentences
\item Learning to prove formal sentences
\end{enumerate}
\item Do they fit the objectives of a mathematician?
\begin{itemize}
\item Use of symbolic quantificators in mathematical prose is low
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Using Natural deduction as a guideline}
\begin{itemize}
\item Natural deduction is a good model of what students should master
\item Each connective has an introduction rule and an elimination rule
\item Proofs rely on the existence of a {\em context} and a conclusion
\begin{itemize}
\item A sequent in proof theory
\item The context is a collection of known facts
\item a {\em conclusion} is a formula that should be proved
\end{itemize}
\item Proof assistants have a proof mode which help organize the activity
\item Some proof assistants make the proof structure apparent
\item Others take a very procedural look-and-feel
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Examples of natural deduction rules}
\[\frac{\Gamma, A, B \vdash C}{\Gamma, A \wedge B \vdash C} \qquad 
\frac{\Gamma \vdash A \quad \Gamma \vdash B}{\Gamma \vdash A \wedge B}\]
\[\frac{\Gamma, A \vdash B}{\Gamma\vdash A \rightarrow B} \qquad
\frac{\Gamma \vdash A \quad \Gamma \vdash A \rightarrow B}{\Gamma \vdash B}\]
\begin{itemize}
\item the horizontal line means ``if what is on top holds, then what is in the bottom holds''
\item the turnstile \(\vdash\) ``from the context on the left, one can deduce the formula on the right''
\item The first two rules explain the ``and'' logical connective
\item To a student that does not grasp syntax, these rules are hard to explain
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Handling universal quantification}
\[\frac{\Gamma \vdash P(x)}{\Gamma\vdash \forall x. P(x)}
 \hbox{x does not appear in \(\Gamma\)}\qquad 
\frac{\strut}{\Gamma, \forall x. P(x) \vdash P(e)}\]
\begin{itemize}
\item Introduction rule: fix an arbitrary object and show that it satisfies
the quantified predicate
\item Elimination rule: Combine with any object and obtain a specialized fact
\item Both introduction rule and elimination rule represent an effort for
newcomers
\item Type theoretical twist: the arbitrary object for the introduction rule is
a fresh ``variable'' in the context, with a specific type
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The takeaway about natural deduction}
\begin{itemize}
\item A good model of explanation in natural language
proofs
\item Some students resent the ``redundant'' feel
\item Some other aspect require that one takes time to master them
\item A good handling of connectives to understand concepts in analysis and
elementary topology
\begin{itemize}
\item Understanding that \(\forall\cdots\exists\cdots\) is not the same as
\(\exists\cdots\forall\cdots\)
\item \(\varepsilon-\delta\) proofs for continuity
\item Uniform continuity
\end{itemize}
\item Proof assistant teachers dedicate a course to this
\item Mathematicians are often reluctant to devote time to the topic
\item Proof assistants prescribe while mathematicians give counter-examples
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Expected difficulties with Coq}
\begin{itemize}
\item The context can be puzzling for students
\begin{itemize}
\item The context can contain objects of different nature: numbers, hypotheses
\item The same notation is used to mean ``is a member of a type'' and
 ``is a proof of''
\item Making the proof assistant treat these pieces of data differently is
one typing operation away
\end{itemize}
\item introduction and elimination rules are not treated uniformly
\begin{itemize}
\item Distinction between minimal logic (universal quantification and implication) and other connectives
\item Encoding of other logical connectives as inductive constructs
\item Induction apparatus to encode elimination rules: {\tt destruct}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example existential quantification elimination}
Current goal
\begin{verbatim}
H : exists k : nat, P k
==========================
C
\end{verbatim}
Action by the user
\begin{verbatim}
destruct H as [k P_k_holds].
\end{verbatim}
New goal
\begin{verbatim}
k : nat
P_k_holds : P k
==========================
C
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Existing solutions}
\begin{itemize}
\item Provide specific subsets of tactics for verbose proofs
\begin{itemize}
\item Done for example in Lean Verbose or Coq waterproof
\end{itemize}
\item Recover ``mock natural language'' look and feel (but repetitive prose)
\end{itemize}
\end{frame}
\begin{frame}
\usebeamercolor{fg}{frametitle}{\Large Curry Howard Isomorphism}
\end{frame}
\begin{itemize}
\item Logical connective have a ``computing'' flavor
\item Type theory exploits it, imposes it
\item Positive aspect: students can add a new perception to theorems
\begin{itemize}
\item Tools that you can combine to obtain a proof
\end{itemize}
\item Negative aspect: the same notation for different concepts
\begin{itemize}
\item {\tt \(x\): \(A\)} has two meanings
\item \(A \rightarrow B\) has two meanings
\item Moreover, the logical interpretation only holds if all functions are total
\end{itemize}
\end{itemize}
\begin{frame}
\begin{center}
\usebeamercolor[fg]{frametitle}{\Large Domains of definition}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Functions and domains of definition}
\begin{itemize}
\item In mathematics every function has a domain
\begin{itemize}
\item Example, the domain of definition of \(x \mapsto x ^ {-1}\) is
 \({\mathbb R} - \{0\}\)
\end{itemize}
\item Functions in type theory have to be defined on the complete input type
\item The concept of domain of definition seldom appears
\item In practice, this means that we use the same notation for slightly
different objects
\item It takes a little adaptation, but mathematicians using Mathlib are ok
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{An unsatisfactory solution: rely on depend types}
\begin{itemize}
\item Old version of mathematical libraries in Coq use dependent types
\item For example an inverse function would take two arguments
\begin{enumerate}
\item As first argument the object to invert
\item As second argument a proof that the object is invertible
\end{enumerate}
\item This turned out to be impractical for ambitious mathematics
\item Also too unwieldy for a learning context
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Impact about proofs}
\begin{itemize}
\item In a way, mathematical work starts by describing the domain of definition
\item Then all operations are performed assuming the verification happened
\item In Type Theory practice, the conditions of being inside the domain of
definition only pop-up in theorems stating properties of the function's value
\item When reading theorem statements, Mathematicians can recognize
well-formedness
\item Still the concept of domain of definition is not supported
\end{itemize}
\end{frame}
\begin{frame}
\begin{center}
\usebeamercolor[fg]{frametitle}
{\Large Numerous number types}
\end{center}
\end{frame}
\begin{frame}
\frametitle{A foundational approach to numbers}
\begin{itemize}
\item Inspired from proof theory: constructs objects from just set theory
\item Start with the empty set and the operation of definition the set of parts
\item Construct natural numbers, then integers, then rational numbers,
\item Then construct the real numbers
\item A similar approach in proof assistants, relying on inductive types
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Natural numbers considered harmful}
\begin{itemize}
\item Natural numbers are the archetypal recursive inductive type
\item A very good tool to understand aspects of proof theory
\item However they behave badly for education purposes:
\begin{itemize}
\item There are three ways to add 1 to a number
\item Subtraction is crooked : \(3 - 5 = 0\), \(5 + (3 - 5) = 5\)!
\item Half the input type is irregular
\end{itemize}
\item My position: natural numbers should not be present in a first
course with proof assistant support
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Avoiding the type of natural numbers}
\begin{itemize}
\item Preliminary experiment
\item Define the subset of real numbers that are natural numbers,
\begin{itemize}
\item Using inductive definitions, epsilon operator, etc.
\item The type of natural numbers can be used in the definition
\item The interface should hide the type
\end{itemize}
\item Provide or prove an induction principle
\item Provide a recursor restricted to real numbers inside the natural subset
\item Rely on arithmetic operations already provided for real numbers
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Take away: hiding foundational work}
\begin{itemize}
\item Foundational work is great for consistency
\item However, it is sometimes to complex for teaching purposes
\item A carefully chosen set of concepts can help students
\item Even so, the type theoretical approach may prevent constructing a good
learning tool
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
