\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\definecolor{myblue}{rgb}{0.1,0.1,0.6}
\definecolor{myblue2}{rgb}{0.3,0.3,0.8}
\definecolor{darkgreen}{rgb}{0.01,0.3,0.0}
\setbeamercolor{coloredboxstuff}{fg=white,bg=myblue2}
\newcommand{\coqand}{\texttt{/\char'134}}
\newcommand{\coqor}{\texttt{\char'134/}}
\setbeamertemplate{footline}[frame number]
\title{Can we use Coq to teach Mathematics?}
\author{Yves Bertot}
\date{June 2022}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Increasing success of computer-verified proofs}
\begin{itemize}
\item A strong complement to other techniques to guarantee software quality
\item Sometimes useful for modern mathematics
\begin{itemize}
\item cf. Helfgott's proof of Ternary Goldbach conjecture\\
Need for guaranteed computation of integrals
\[\int_{-\infty}^{\infty} \frac{(0.5 \cdot \ln(\tau^2 + 2.25) + 4.1396 + \ln \pi) ^ 2}
{0.25 + \tau ^ 2}{\rm{d}}\tau\]
\item Mahboubi, Melquiond \& Sibut-Pinote showed mistakes in known software 
 (relative error larger than \(10 ^ {-5}\))
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{How to guarantee the correctness of computations?}
\begin{itemize}
\item Return intervals instead of approximations
\item Decompose the  domain of study in small tractable chunks
\item Enclose intricate functions between polynomials
\item Rely on know results of calculus
\item Use logic to avoid leaving odd cases behind
\end{itemize}
\end{frame}
\begin{frame}
\includegraphics[scale=0.5]{Helfgott_integral.pdf}
\end{frame}

\begin{frame}
\frametitle{Mathematical Education?}
\begin{itemize}
\item Help students acquire mathematical rigor
\item Benefit from accumulated knowledge in Coq libraries
\item Examples here: knowledge in calculus and arithmetics
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example : Prove that for every positive x, $\sin x < x$}
\begin{center}
\includegraphics[scale=0.3]{x_sin.pdf}
\end{center}
Floating point approximations are inclusive where the curves touch
\end{frame}
\begin{frame}
\frametitle{Example : Prove that for every positive x, $\sin x < x$}
\begin{itemize}
\item Decompose domain \(0 < x\) into two parts
\item for \(1 < x\), use the fact that \(\sin x \leq 1\)
\item for \(0 < x \leq 1\) consider the function \(x - \sin x\), and show
it is \(0\) at 0 and increasing between 0 and 1
\begin{itemize}
\item Mean Value Theorem (in French: {\em accroissements finis})
\item Derivative of \(x - \sin x\) is \(1 - \cos x\)
\item \(0 < 1 - \cos y\) for \(0 < y < \pi\)
\item Use the fact that \(1 < \pi\)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Verification by computer}
\begin{itemize}
\item Write intermediate steps of the proof
\item Computer checks that all steps fit together for the claim
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example}
\begin{alltt}
\textcolor{myblue}{Lemma sinx_ltx x : 0 < x -> sin x < x.}
Proof.
\textcolor{myblue}{assert (1 < x -> 0 < x - sin x).}
  \textcolor{myblue}{assert (sin x <= 1)} by check.
  check. (* end of right_part *)
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example: the more difficult part}
\begin{alltt}
\textcolor{myblue}{enough (0 < x <= 1 -> 0 < x - sin x) by check.}
\textcolor{myblue}{assume (0 < x <= 1).}
\textcolor{myblue}{now_prove (sin x < x)}.
\textcolor{myblue}{pose (f := fun y => y - sin y).}
\textcolor{myblue}{assert (f(0) = 0).}
    \textcolor{red}{unfold f.}
    \textcolor{myblue}{assert (sin 0 = 0) by apply sin_0.}
    check.
\textcolor{myblue}{enough (0 < f(x)).}
    \textcolor{red}{unfold f in *.}
    check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example: the more difficult part (2)}
Using a known result
\begin{beamercolorbox}[wd=\textwidth,sep=1em]{coloredboxstuff}
\begin{alltt}
Mean_Value_Theorem :
forall (f f' : R -> R) (a b : R),
   a < b \coqand{}
   (forall c, a <= c <= b -> derivative(f, c, f'(c)))
   ->
   exists c, f(b) - f(a) = f'(c) * (b - a) \coqand{}
             a < c < b.
\end{alltt}
\end{beamercolorbox}
\begin{alltt}
\textcolor{myblue}{assert (exists c, (f(x) - f(0) =
             (1 - cos c) * (x - 0)) \coqand{} 0 < c < x)
    as [c derivative_in_c_and_c_between_0_x]}.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example: the more difficult part (3)}
\begin{beamercolorbox}[wd=\textwidth,sep=1em]{coloredboxstuff}
\begin{alltt}
Mean_Value_Theorem :
forall (f f' : R -> R) (a b : R),
   a < b \coqand{}
   (forall c, a <= c <= b -> derivative(f, c, f'(c)))
   ->
   exists c, f(b) - f(a) = f'(c) * (b - a) \coqand{}
             a < c < b.
\end{alltt}
\end{beamercolorbox}
\begin{alltt}
    apply Main_value_Theorem.
    split.
        check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{The derivative property}
\begin{alltt}
\textcolor{myblue}{now_prove (forall c, 0 <= c <= x ->
       derivative(f, c, 1 - cos c)).}
    Fix c.
    \textcolor{myblue}{assume (0 <= c <= x).}
    \textcolor{red}{derivative_prover}.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example: wrapping up}
\begin{alltt}
\textcolor{myblue}{now_prove (0 < f(x) - f(0)).}
\textcolor{myblue}{enough (0 < (1 - cos c) * (x - 0))} by check.
\textcolor{myblue}{enough (cos c < 1)} by check.
\textcolor{myblue}{assert (cos 0 = 1)} by \textcolor{red}{apply cos_0}.
\textcolor{myblue}{enough (cos c < cos 0)} by check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example : wrapping up (2)}
Use another known fact
\begin{beamercolorbox}[wd=\textwidth,sep=1em]{coloredboxstuff}
\begin{alltt}
{cos_decreasing :
forall x y : R, 0 <= x <= PI \coqand{} 0 <= y <= PI \coqand{} x < y
   -> cos y < cos x}
\end{alltt}
\end{beamercolorbox}
\begin{alltt}
\textcolor{myblue}{apply cos_decreasing.}
\textcolor{red}{Fail check.}
\textcolor{myblue}{now_prove (0 <= 0 <= PI \coqand{} 0 <= c <= PI \coqand{} 0 < c).}
repeat split; try check.
\textcolor{myblue}{now_prove (c < PI)}.
\textcolor{myblue}{assert (3.14 <= PI <= 3.15)} by check.
check.
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Where is the catch?}
Students should learn maths, not {\em how to use Coq}
\begin{itemize}
\item Number of different commands to know
\item Finding names of theorems
\item Expertise in what the automated checker can do
\item Knowledge in logic and proof theory
\item Difference of idioms with type theory (syntax and types-as-sets)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Proof commands}
\begin{itemize}
\item {\tt assume}, {\tt now\_prove}, {\tt Fix}, {\tt check}, {\tt derivative\_prover} were defined for this example
\item {\tt assert}, {\tt enough}, {\tt pose}, {\tt apply}, {\tt unfold}, {\tt split}, and a few others are in the Coq traditional proof commmands
\item More should be defined or fine-tuned for the specific student public
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Finding names of theorems}
\begin{itemize}
\item Importance of search facilities (by name or by pattern)
\item Restrict search to specific corpora
\item Names of concept are also difficult to find
\item Overwhelming amouts of information
\begin{itemize}
\item {\tt Search \"deriv\".} returns 2500 lines
\item {\tt Search \"deriv\" (\_ - \_).} does not find the mean value theorem
\item {\tt is\_derive} is not defined for functions from \({\mathbb R}\) to \({\mathbb R}\)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Expertise in what the automated check can do}
\begin{itemize}
\item {\tt check} is a combination of:
\begin{itemize}
\item Automatic apply of a theorem database and asserted facts
\item Equality reasoning using field properties
\item Linear arithmetic
\item Interval reasoning (comparisons with rational bounds)
\end{itemize}
\item Expert users can predict success or failure
\item How puzzling for students?
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Rationale for automation in a teaching context}
\begin{itemize}
\item Automation should make ``obvious steps'' quick
\item Should not replace student's practice
\item Poor match to human intelligence
\begin{itemize}
\item Brute force through long tedious computations
\item Bad at combining easy elementary step
\end{itemize}
\item Visible in proof of \(c < \pi\) from example
\begin{itemize}
\item Linear arithmetic can combine \(c < 1\) and \(1 < \pi\), but does not know the latter
\item {\tt interval} can prove \(1 < \pi\) but does not combine with other facts
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Logic and proof theory}
\begin{itemize}
\item Is some part of the curriculum dedicated to logic?
\item Know the basic elementary steps of reasoning with common logical connectives:
\begin{itemize}
\item and {\tt \coqand{}}, or {\tt \coqor{}}, implication {\tt ->},
negation {\verb+~+}
\item Universal and existential quantification need training too
\item Coq provides specific proof commands for these connectors
\item Should there be a module dedicated to that?
\item Automation can help, but impact on learning is unclear
\end{itemize}
\item Different levels of detail for different courses
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example of existential quantification}
\begin{itemize}
\item Proving the existence of an object does not give access to
 this object
\item In our example proof, we used ``{\tt assert ... as [c der...]} to
combine proof and access
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Impact of type theory}
\begin{itemize}
\item Types as a sanity check are helpful for students
\item Foundational approach of types lead to different types of numbers
\begin{itemize}
\item natural numbers, integers, rational numbers, real numbers : all different
\item Partial solutions provided by coercions
\end{itemize}
\item Alternative approach require new work
\begin{itemize}
\item Work with large type (e.g. \(\mathbb{R}\) or \(\mathbb{C}\)
\item View all other types as subsets
\item Include stability laws (e.g. sum of natural numbers is a natural number)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Structure of an example proof by induction on \(\mathbb{N}\)}
\[\sum_{i \in \{0 .. (n - 1)\}} i = n (n - 1) / 2\]
\begin{itemize}
\item Proof by induction on \(n\)
\item Easy to verify by computation when \(n = 0\)
\item Assume the property is already satisfied at \(n\), prove it is
satisfied for \(n + 1\).
\begin{eqnarray*}
\sum_{i \in \{0 .. ((n + 1) - 1)\}} i &=& \sum_{i \in \{0 .. (n - 1)\}} + n\\
&\lefteqn{\hbox{by induction hypothesis}}\\
& = & n (n - 1) / 2 + n\\
&\lefteqn{\hbox{by algebraic computation}}\\
& = & (n + 1) ((n + 1) - 1) / 2
\end{eqnarray*}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Teaching material for a proof by induction}
\begin{itemize}
\item A property {\tt is\_nat} of real numbers: {\em \(\in \mathbb{N}\)}
\begin{itemize}
\item \(0, 1 \in {\mathbb{N}}\),\qquad \(\forall n, m\in {\mathbb{N}}, (n + m) \in {\mathbb{N}}\)
\end{itemize}
\item A function \({\tt sumn}\) to represent \(\sum_{i \in \{0 .. (n - 1)\}} i\) with two defining properties:
\begin{itemize}
\item \({\tt sumn}(0) = 0\) \qquad \(\forall n \in {\mathbb{N}}, {\tt sumn} (n + 1) = {\tt sumn}(n) + (n + 1)\)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction}
\begin{alltt}
\textcolor{myblue}{Section exercicse_on_sum.

Variable is_nat : R -> Prop.

Hypothesis isnat0 : isnat 0.

Hypothesis isnat1 : isnat 1.

Hypothesis isnat_add : forall x y, isnat(x) \coqand{} isnat(y) ->
   isnat(x + y).

Hypothesis isnat_ind : 
  forall P : R -> Prop,
    (P(0) \coqand{}
     (forall x, isnat(x) \coqand{} P(x) -> P(x + 1))) ->
  forall x, isnat(x) -> P(x).}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction (2)}
\begin{alltt}
\textcolor{red}{Ltac prove_by_nat_induction := apply isnat_ind.}

\textcolor{myblue}{Variable sumn : R -> R.

Hypothesis sumn0 : sumn 0 = 0.

Hypothesis sumn_1 : forall n, sumn (n + 1) = sumn n + n.}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction (3)}
\begin{alltt}
\textcolor{myblue}{Lemma sumn_eq : forall n, isnat n -> 
    sumn n = n * (n - 1) / 2.}
Proof.
prove_by_nat_induction.
\textcolor{myblue}{assert (base_case : sumn 0 = 0 * (0 - 1) / 2).}
    check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction (4)}
\begin{alltt}
\textcolor{myblue}{assert (step_case : forall n, (isnat n \coqand{}
            sumn n = n * (n - 1) / 2) ->
            sumn (n + 1) = (n + 1) * ((n + 1) - 1) / 2).}
Fix n.
\textcolor{myblue}{assume (isnat n \coqand{} sumn n = n * (n - 1) / 2).
now_prove (sumn (n + 1) = (n + 1) * ((n + 1) - 1) / 2).
assert (eq_for_n : sumn n = n * (n - 1) / 2).}
    check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction (4)}
Attempt to simulate the sequence of justified equalities
\begin{alltt}
\textcolor{red}{remember (sumn (n + 1)) as computation eqn: cval.}
\textcolor{myblue}{assert (cval1 : computation = sumn n + n).}
        rewrite cval, sumn_1; check.
\textcolor{myblue}{assert (cval2 : computation = (n * (n - 1) / 2 + n)).}
        rewrite cval1, eq_for_n; check.
    check.
check.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq example on induction}
\begin{center}
\LARGE Demo time!
\end{center}
\end{frame}
\begin{frame}
\frametitle{On induction}
\begin{itemize}
\item Coq has a genral theory of induction of inductive types and predicates
\begin{itemize}
\item Introduction rules (akin to {\tt isnat0}, {\tt isnat1},
 and {\tt isnat\_add}) given by the user
\item Induction principle deduced by the Coq system
\end{itemize}
\item A cornerstone of the whole logical system
\begin{itemize}
\item explains even logical connectives and equality
\end{itemize}
\item For students of mathematics, this is often confusing
\begin{itemize}
\item For example, elimination rules for logical connectives
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Conclusion}
\begin{itemize}
\item Design geared towards least effort of the proof writer
\item Scripts are seldom read standalone
\item Proof commands {\tt assume}, {\tt now\_prove} are not idiomatic
\item Formulas rely on different syntactic conventions
\item For math education, we need to set up a new idiom
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
